package flyweight

import "fmt"

type SoldierTest struct {
	name string
}

func newSoldierTest(name string) *SoldierTest {
	return &SoldierTest{
		name: name,
	}
}

func (s *SoldierTest) Promote(context *Context) {
	fmt.Printf("%s %s promoted soldier test %d\n", s.name, context.id, context.star)
}
package main

import (
	flyweight "flyweight/pkg"
	"fmt"
	"strconv"
)

func main() {
	factory := flyweight.NewFactory()

	for i := 1; i <= 15; i++ {
		startContext := flyweight.NewContext(strconv.Itoa(i), 2)
		soldier := factory.CreateSoldier("food man")
		soldier.Promote(startContext)
	}

	for i := 1; i <= 15; i++ {
		startContext := flyweight.NewContext(strconv.Itoa(i), 2)
		soldier := factory.CreateSoldier("sea man")
		soldier.Promote(startContext)
	}

	for i := 1; i <= 15; i++ {
		startContext := flyweight.NewContext(strconv.Itoa(i), 2)
		soldier := factory.CreateSoldier("test man")
		soldier.Promote(startContext)
	}

	fmt.Printf("Number of storage soldier map: %d\n", factory.GetSize())
}